# README #

### What is this repository for? ###

* Calls differences using a windowless region caller between two groups for BS-Seq
* Can specify a permutation test

### How do I get set up? ###

To compile type:


```
#!shell

g++ main.cpp
```

To run:


```
#!shell

./dmr.out -i <input_filename> -g <comma separated groupings e.g. 1,0,1,1,0,0> > <output.txt>
```

