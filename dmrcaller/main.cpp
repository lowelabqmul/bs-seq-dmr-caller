//
//  main.cpp
//  dmrcaller
//
//  Created by Robert Lowe on 30/07/2013.
//  Copyright (c) 2013 Robert Lowe. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm> 
#include <getopt.h>
#include "file_operations.h"
#include "stats.h"

struct statistics {
    std::string chr;
    int spos;
    int epos;
    bool meth;
    std::vector<double> pvals;
    std::vector<double> qvals;
    std::vector<double> d;
};

double chisq_setup(std::vector<std::string> &tokens,std::vector<int> & groups){
    
    int indx=0;
    std::vector<int> mat;
    for(int i=0; i<4; i++){
        mat.push_back(0);
    }
    for(int i=2; i<tokens.size(); i+=2){
        mat[groups[indx]*2+1]+=atoi(tokens[i+1].c_str());
        mat[groups[indx]*2]+=atoi(tokens[i].c_str());
        indx++;
    }
    
    return(chisq(mat[0], mat[1]-mat[0], mat[2], mat[3]-mat[2], true));

}

int myrandom (int i) { return std::rand()%i;}

double calculate_meth_diff_perm(std::vector<std::string> &tokens,std::vector<int> & groups){

    double teststat=chisq_setup(tokens,groups);
    std::vector<int> permgroups=groups;
    int cnt=0;
    for(int i=0; i<1000; i++){
        std::random_shuffle(permgroups.begin(), permgroups.end(),myrandom);
        if(teststat>=chisq_setup(tokens,permgroups))
            cnt+=1;
    }
    return(1-cnt/1000.0);
}



double calculate_meth_diff(std::vector<std::string> &tokens,std::vector<int> & groups){
    
    int indx=0;
    std::vector<double> avmeth,methcnts;
    avmeth.push_back(0.0);
    avmeth.push_back(0.0);
    methcnts.push_back(0.0);
    methcnts.push_back(0.0);

    for(int i=2; i<tokens.size(); i+=2){
        avmeth[groups[indx]]+=(double)atoi(tokens[i].c_str())/(double)atoi(tokens[i+1].c_str());
        methcnts[groups[indx]]+=1;
        indx++;
    }

    return(avmeth[0]/methcnts[0]-avmeth[1]/methcnts[1]);
}


void runtest(std::vector<std::string> &tokens,int pos, statistics &current,std::vector<int> & groups, int maximum_distance_between_cpgs,int minimum_window_size){

    double diff=calculate_meth_diff(tokens,groups);
    double permp=chisq_setup(tokens,groups);

    
    if(pos==0 || atoi(tokens[1].c_str())-pos>maximum_distance_between_cpgs){
        current.chr=tokens[0];
        current.spos=atoi(tokens[1].c_str());
        current.meth=diff>0;
        current.pvals.clear();
        current.qvals.clear();
        current.d.clear();
    }
    
    if( (diff<0 && current.meth) || (diff>0 && !current.meth)){
        if(pos-current.spos>minimum_window_size){
            double sum=0.0;
            double mean_diff=0.0;
            for(int i=0; i<current.pvals.size(); i++){
                sum+=log(current.pvals[i]);
                mean_diff+=current.d[i];
            }
            double gin,gim,gip;
            incog(current.pvals.size(), 0.5*-2*sum, gin, gim, gip);
            std::cout << tokens[0] << " " << current.spos << " " << pos << " " << mean_diff/current.d.size() << " " << " " << current.pvals.size() << " " << 1-gip << std::endl;
        }
        current.chr=tokens[0];
        current.spos=atoi(tokens[1].c_str());
        current.meth=diff>0;
        current.pvals.clear();
        current.qvals.clear();
        current.d.clear();
    }

    current.pvals.push_back(permp);
    current.qvals.push_back(permp);
    current.d.push_back(diff);
    
}

void runtest_permutation(std::vector<std::string> &tokens,int pos, statistics &current,std::vector<int> & groups, int maximum_distance_between_cpgs, int minimum_window_size){
    
    double diff=calculate_meth_diff(tokens,groups);
    double pvalue=calculate_meth_diff_perm(tokens,groups);

    if(pos==0 || atoi(tokens[1].c_str())-pos>maximum_distance_between_cpgs){
        current.chr=tokens[0];
        current.spos=atoi(tokens[1].c_str());
        current.meth=diff>0;
        current.pvals.clear();
        current.qvals.clear();
        current.d.clear();
    }
    
    if( (diff<0 && current.meth) || (diff>0 && !current.meth)){
        if(pos-current.spos>minimum_window_size){
            double sum=0.0;
            double mean_diff=0.0;
            for(int i=0; i<current.pvals.size(); i++){
                sum+=log(current.pvals[i]);
                mean_diff+=current.d[i];
            }
            double gin,gim,gip;
            incog(current.pvals.size(), 0.5*-2*sum, gin, gim, gip);
            std::cout << tokens[0] << " " << current.spos << " " << pos << " " << mean_diff/current.d.size() << " " << " " << current.pvals.size() << " " << 1-gip << std::endl;
        }
        current.chr=tokens[0];
        current.spos=atoi(tokens[1].c_str());
        current.meth=diff>0;
        current.pvals.clear();
        current.qvals.clear();
        current.d.clear();
    }
    
    current.pvals.push_back(pvalue);
    current.qvals.push_back(pvalue);
    current.d.push_back(diff);

}


void readfile(std::string filename,std::string output, bool permutation, int maximum_distance_between_cpgs, int minimum_window_size, std::vector<int> & groups){
    
    statistics current;
    int pos=0;
    std::string curchr="";
    std::ifstream myfile(filename.c_str());
    if(myfile.is_open()){
        std::string line;
        getline(myfile,line);
        while(getline(myfile,line)){
            std::vector<std::string> tokens=tab_split_line(line);
            if(curchr!=tokens[0]){
                std::cerr << tokens[0] << std::endl;
                curchr=tokens[0];
                pos=0;
            }
            if(pos>atoi(tokens[1].c_str())){
                std::cerr << "File not sorted. Previous position: " << pos << std::endl;
                std::cerr << line << std::endl;
                exit(0);
            }
            
            int cnt=0;
            for(int i=2; i<tokens.size(); i+=2){
                //Check all samples have at least a single read.
                if(atoi(tokens[i].c_str())>1)
                    cnt+=1;
            }
            if(cnt==(tokens.size()-2)/2.0){
                if((tokens.size()-2)/2.0!=groups.size()){
                    std::cerr << "Grouping input did not match number of samples" << std::endl;
                    std::cerr << "Number of samples = " << (tokens.size()-2)/2.0 << std::endl;
                    std::cerr << "Number of groups ids = " << groups.size() << std::endl;
                    exit(0);
                }
                if(permutation){
                    if(groups.size()>2){
                        runtest_permutation(tokens,pos,current,groups,maximum_distance_between_cpgs,minimum_window_size);
                    }
                    else{
                        std::cerr << "Can not run permutation on 2 samples" << std::endl;
                    }
                }
                else{
                    runtest(tokens,pos,current,groups,maximum_distance_between_cpgs,minimum_window_size);
                }
                pos=atoi(tokens[1].c_str());
            }
        }
    }else{
        std::cerr << "Couldn't read file" << std::endl;
    }
    
    
    
}


int main (int argc,  char * argv[])
{
    std::string grouping="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1",filename="/Users/robertlowe/Desktop/QMUL/fluidigm/meth.txt",output;
    int maximum_distance_between_cpgs=1000,minimum_window_size=0;
    bool permutation;
    int ch;


    while ((ch = getopt(argc, argv, "i:g:w:m:p")) != -1) {
		switch (ch) {
            case 'i':
				filename=optarg;
				break;
            case 'g':
                grouping=optarg;
                break;
            case 'p':
                permutation=true;
                break;
            case 'w':
                minimum_window_size=atoi(optarg);
                break;
            case 'm':
                maximum_distance_between_cpgs=atoi(optarg);
                break;


        }
    }
    
    
    std::vector<int> groups;
    int pos;
    while( (pos = (int)grouping.find(","))!=std::string::npos)
    {
        std::string line2=grouping.substr(0,pos);
        groups.push_back(atoi(line2.c_str()));
        grouping = grouping.substr(pos+1);
    }
    if(groups.size()==0){
        std::cerr << "Found no groups. Please provide list of groups as comma separated string e.g. 0,0,1,0,1" << std::endl;
        exit(0);
    }
    groups.push_back(atoi(grouping.c_str()));
    readfile(filename,output,permutation,maximum_distance_between_cpgs,minimum_window_size,groups);
    return 0;
}

